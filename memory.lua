local unit = require 'unit'
local memory = {}

function memory.get()
  local file = io.open('/proc/meminfo', 'r')
  local line = file:read()
  output = {}
  while line do
    local key, value, suffix = line:match('^([^:]+):%s+(%d+)%s*([kKmMgGtT]?i?)B?$')
    if key then
      output[key] = tonumber(value) * unit.get_multiplier(suffix)
    end
    line = file:read()
  end
  file:close()
  return output
end

return memory
