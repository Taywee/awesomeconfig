local unit = {}

function unit.get_multiplier(suffix)
  if suffix:match('^[kK]') then
    return 1000
  elseif suffix:match('^[kK][iI]') then
    return 1024
  elseif suffix:match('^[mM]') then
    return 1000000
  elseif suffix:match('^[mM][iI]') then
    return 1048576
  elseif suffix:match('^[gG]') then
    return 1000000000
  elseif suffix:match('^[gG][iI]') then
    return 1073741824
  elseif suffix:match('^[tT]') then
    return 1000000000000
  elseif suffix:match('^[tT][iI]') then
    return 1099511627776
  else
    return 1
  end
end

-- Scale unit up to acceptable display
function unit.scale(value)
  for i, suffix in ipairs{'', 'Ki', 'Mi', 'Gi', 'Ti'} do
    local scaled = value / unit.get_multiplier(suffix)
    if scaled < 1024 then
      return scaled, suffix
    end
  end
  -- Don't know wtf
  return value, 'ERR'
end

function unit.string(value)
  local scaled, suffix = unit.scale(value)
  return ('%.02f %sB'):format(scaled, suffix)
end

return unit
